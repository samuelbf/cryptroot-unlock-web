#!/bin/busybox ash

# Unlock encrypted volumes with a web server.
#
# Copyright © 2019 Samuel Bizien Filippi <samuel.bizien@laposte.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -ue

echo ""
cat <<HTML
<html><head><title>Unlocking disk</title>
<style type="text/css">
body {
    font-family: monospace;
} 
div, form {
    margin: auto;
    margin-top: 120px;
    width: 300px;
    border: #999 1px solid;
    border-radius: 5px;
}

h1 {
	font-size: 1.2em;
	font-weight: bold;
	margin: 0;
	padding: 3px;
	border-bottom: #999 1px solid;
}

p {
    margin: 20px;
    font-weight: bold;
}

.ok {
    border: #0A0 3px solid;
}

input, button {
	display: block;
	width: 80%;
	margin: auto;
	margin-top: 20px;
	margin-bottom: 20px;
}
</style>
</head>
<body>
HTML

PASSFIFO=/tmp/cryptroot_pass
CRYPTROOT_OUTPUT=/tmp/cr_output
CRYPTROOT_DEVICE=/tmp/cr_device

# Processing post values
if [ "$REQUEST_METHOD" = "POST" ]; then
	if [ ! -p "$PASSFIFO" ]; then
		echo "<p>Password already transmitted. <a href=index.cgi>retry</a></p></body></html>"
		exit 0;
	fi
	# Feeding password to cryptroot-unlock 
	read -rs -n "$CONTENT_LENGTH" QUERY_STRING
	busybox httpd -d "${QUERY_STRING:9}" > "$PASSFIFO"
	# Reading cryptroot answer
	echo "<div><p>"
	cat "$CRYPTROOT_OUTPUT"
    echo "</p></div>"
    rm -f "$PASSFIFO" "$CRYPTROOT_OUTPUT" "$CRYPTROOT_DEVICE"
fi

if [ ! -p "$PASSFIFO" ]; then
	# Starting cryptroot-unlock :
	mkfifo "$PASSFIFO" "$CRYPTROOT_OUTPUT"
	cat "$PASSFIFO" | cryptroot-unlock > "$CRYPTROOT_OUTPUT" 2>&1 &
	head -1 "$CRYPTROOT_OUTPUT" > "$CRYPTROOT_DEVICE"
fi

# If nothing in cryptroot output : it means that device was successfully unlocked.
device=$(cat "$CRYPTROOT_DEVICE")
if [ -z "$device" ]; then
    echo "<div class=ok><p>All devices unlocked</p></div></body></html>"
    exit 0;
fi
# Else, displaying form :
echo "<form method=post><h1>"
cat "$CRYPTROOT_DEVICE"
cat <<HTML
</h1>
<input name=password type=password placeholder=password autocomplete=off />
<button type=submit>Unlock</button>
</form>
</body></html>
HTML

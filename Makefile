#!/usr/bin/make
PREFIX = /usr

install:
	mkdir -p $(DESTDIR)$(PREFIX)/share/cryptroot-unlock-web /usr/share/initramfs-tools/hooks /usr/share/initramfs-tools/scripts/init-premount /usr/share/initramfs-tools/scripts/init-bottom	
	cp -r www $(DESTDIR)$(PREFIX)/share/cryptroot-unlock-web/
	cp scripts/initramfs.hook.cryptroot-unlock-web /usr/share/initramfs-tools/hooks/cryptroot-unlock-web
	cp scripts/cryptroot-unlock-web /usr/share/initramfs-tools/scripts/init-premount/cryptroot-unlock-web
	cp scripts/cryptroot-unlock-web.stop /usr/share/initramfs-tools/scripts/init-bottom/cryptroot-unlock-web
	cp -r etc /etc/cryptroot-unlock-web

uninstall:
	rm -R $(DESTDIR)$(PREFIX)/share/cryptroot-unlock-web
	rm /usr/share/initramfs-tools/hooks/cryptroot-unlock-web
	rm /usr/share/initramfs-tools/scripts/init-premount/cryptroot-unlock-web
	rm /usr/share/initramfs-tools/scripts/init-bottom/cryptroot-unlock-web

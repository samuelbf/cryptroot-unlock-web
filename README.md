# Cryptroot unlock web

Enter unlocking key for LUKS-encrypted root volumes with a web interface.

## Status

This is an early draft :

- TLS protection does not work (stunnel starts extremely slow, in 5 minutes or more, which makes it unusable), and this functionality without TLS is extremely dangerous
- It is Debian-specific : a more generic solution would work with [dracut](https://dracut.wiki.kernel.org/index.php/Main_Page) and maybe [clevis](https://github.com/latchset/clevis)

Note that I take no commitment to improve this program.

## How to install it

Tested on Debian 10. No information about how it would work with other linuxes or other versions.

Dependencies :

- initramfs-tools
- stunnel
- make (for install)

Commands to make it work :
```sh
# make install
# update-initramfs -u
```

To uninstall, run :
```sh
# make uninstall
# update-initramfs -u
```
